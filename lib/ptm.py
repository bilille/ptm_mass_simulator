"""Class to determine all possible combinations of PTMs (regardless their position)
that can be present in a peptid given its sequence"""

import os
import sys
import itertools

class PTM():

    def __init__(self, ptm_file):
        self.possible_ptms = dict()
        self.modifiable_AA = dict()
        self.AA_with_possibly_several_PTMs = dict()     # Ex K
        self.PTMs_affecting_AA_with_possibly_several_PTMs = set()    # Ex: methylation and acetylation (on K)
        self.dubious_AA = set()  # Ex K and R (methylation for both)
        if ptm_file and os.path.isfile(ptm_file):
            ptm_fh = open(ptm_file, 'r')
            header = ptm_fh.readline()
            if header.strip() != "Name\tMonoisotopic_mass_change\tAverage_mass_change\tAA\tFrequency\tActivated":
                raise Exception("PTMs file does not have the good format. Columns must be 'Name', 'Monoisotopic_mass_change', 'Average_mass_change', 'AA', 'Frequency' and 'Activated'")
            for line in iter(ptm_fh.readline, ''):
                fields = line.rstrip().split("\t")
                if len(fields) == 6:
                    if fields[0] == 'Name':
                        continue
                    if fields[5] == 'no':
                        continue
                self.possible_ptms[fields[0]] = {'frequency': fields[4], 'mass': float(fields[1]), 'AA': []}

                list_AA = fields[3].split(',')
                for AA in list_AA:
                    AA_stripped = AA.strip()
                    if AA_stripped:
                        self.possible_ptms[fields[0]]['AA'].append(AA_stripped)
                        if AA_stripped not in self.modifiable_AA:
                            self.modifiable_AA[AA_stripped] = dict()
                            self.modifiable_AA[AA_stripped]['always'] = list()
                            self.modifiable_AA[AA_stripped]['occasional'] = list()
                        self.modifiable_AA[AA_stripped][fields[4]].append(fields[0])
            ptm_fh.close()

            # List the AA that can be affected by several different PTMs
            for AA in self.modifiable_AA:
                if len(self.modifiable_AA[AA]['occasional']) > 1:
                    self.AA_with_possibly_several_PTMs[AA] = self.modifiable_AA[AA]['occasional'].copy()

            # List the PTMs that can affect the AAs that can be affected by several different PTMs
            for AA in self.AA_with_possibly_several_PTMs:
                for PTM in self.AA_with_possibly_several_PTMs[AA]:
                    self.PTMs_affecting_AA_with_possibly_several_PTMs.add(PTM)

            # List all the AA that can be affected by a PTM that can affect the AAs that can be affected by several different PTMs
            for PTM in self.PTMs_affecting_AA_with_possibly_several_PTMs:
                for AA in self.possible_ptms[PTM]['AA']:
                    self.dubious_AA.add(AA)
        else:
            raise Exception("Could not open {} file".format(ptm_file))


    def count_dubious_sites(self, seq):
        nb = 0
        for AA in seq:
            if AA in self.dubious_AA:
                nb += 1
        return nb


    def is_combination_possible(self, combination, nb_max_PTMs):
        nb_PTMs = 0
        for elt in combination:
            fields = elt.split('_')
            if fields[1] in self.PTMs_affecting_AA_with_possibly_several_PTMs:
                nb_PTMs += int(fields[0])
        if nb_PTMs > nb_max_PTMs:
            return False
        return True


    def count_ptms(self, seq):
        """Returns a dict with 2 values:
        - 'always': for PTMs that, if they are present, affect all the AA involved (like carbamidomethylation of C)
        - 'occasional': for PTMs that may not affect all the AA that may have the modification
        'always' is a dict with key=PTM name and value=nb of occurrences
        'occasional' is a dict with key=PTM name and value=list of possible number of occurrences
        (depending on the sequence)
        {'always': {'carbamidomethylation': 1},
         'occasional': {'acetylation': ['0_acetylation', '1_acetylation'],
                        'methylation': ['0_methylation',
                                        '1_methylation',
                                        '2_methylation'],
                        'oxidation': ['0_oxidation', '1_oxidation']}}
        """
        dict_ptms = dict()
        dict_ptms['always'] = dict()
        dict_ptms['occasional'] = dict()
        for aa in seq:
            if aa in self.modifiable_AA:
                if self.modifiable_AA[aa]['always']:
                    for ptm in self.modifiable_AA[aa]['always']:
                        if ptm not in dict_ptms['always']:
                            dict_ptms['always'][ptm] = 0
                        dict_ptms['always'][ptm] += 1
                if self.modifiable_AA[aa]['occasional']:
                    for ptm in self.modifiable_AA[aa]['occasional']:
                        if ptm not in dict_ptms['occasional']:
                            dict_ptms['occasional'][ptm] = ['0_' + str(ptm)]
                        nb_elts = len(dict_ptms['occasional'][ptm])
                        dict_ptms['occasional'][ptm].append("{}_{}".format(nb_elts, ptm))
        return dict_ptms

