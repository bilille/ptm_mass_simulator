#! /usr/bin/env python

import os
import argparse
from Bio import SeqIO

from lib.ptm import *
from lib.molmass import *
from lib.elements import *

##### Define command line ############################################

desc = "Predict the combinations of PTMs and their masses in a given peptid sequence"

command = argparse.ArgumentParser(prog = 'PTM_mass_simulator.py',
    description = desc,
    usage = '%(prog)s [options]')

command.add_argument('-s', '--sequence', nargs = '?',
    type = str,
    help = 'Peptid sequence (mutually exclusive with -f)')

command.add_argument('-f', '--fasta', nargs = '?',
    type = str,
    help = 'Input proteic fasta file (mutually exclusive with -s)')

command.add_argument('--ptm', nargs = '?',
    type = str,
    required = True,
    help = 'path of tabulated file with the allowed PTMs')

command.add_argument('-o', '--output', nargs = '?',
    type = str,
    required = True,
    help = 'path of output CSV file (required)')


##### Read arguments of command line #################################
args = command.parse_args()

if not args.sequence and not args.fasta:
    raise Exception("You must give an input sequence either with --sequence or with --fasta")


##### Main ###########################################################
sequences = dict()

if args.sequence:
    sequences[">sequence"] = args.sequence
else:
    if args.fasta and os.path.isfile(args.fasta):
        for record in SeqIO.parse(args.fasta, "fasta"):
            sequences[record.description] = record.seq
    else:
        raise Exception(str(args.fasta) + " is not found")

ptm_predictor = PTM(args.ptm)

with open(args.output, 'w') as out_fh:
    out_fh.write("id,sequence,mandatory_PTMs,occasional_PTMs,mass\n")
    for id_seq in sequences:
        sequence = sequences[id_seq]
        id_seq = id_seq.lstrip('>')

        # Calculate the number of 'dubious' sites
        nb_sites = ptm_predictor.count_dubious_sites(sequence)

        # Calculate mass with 0 PTM
        formula = Formula('peptide({})'.format(sequence))
        base_mass = formula.isotope.mass

        # Count the max occurences for each PTM
        dict_ptms = ptm_predictor.count_ptms(sequence)

        # Add the masses due to always occuring PTMs
        mandatory_PTMs_list = list()
        for ptm in dict_ptms['always']:
            mandatory_PTMs_list .append("{}_{}".format(dict_ptms['always'][ptm], ptm))
            base_mass += dict_ptms['always'][ptm] * ptm_predictor.possible_ptms[ptm]['mass']

        # Calculate all the combinations with occasional PTMs
        mandatory_PTMs = '|'.join(mandatory_PTMs_list)
        for combination in itertools.product(*dict_ptms['occasional'].values()):
            if ptm_predictor.is_combination_possible(combination, nb_sites):
                mass = base_mass
                combination_str = '|'.join(combination)
                for elt in combination:
                    fields = elt.split('_')
                    mass += float(fields[0]) * float(ptm_predictor.possible_ptms[fields[1]]['mass'])
                out_fh.write("{},{},{},{},{}\n".format(id_seq, sequence, mandatory_PTMs, combination_str, mass))

